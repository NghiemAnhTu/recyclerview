package com.rikkei.tranning.ui;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

public class StudentAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    ArrayList<Student> mListStudent;
    Context mContext;

    // contructer

    public StudentAdapter(ArrayList<Student> mListStudent, Context mContext) {
        this.mListStudent = mListStudent;
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        Context context = viewGroup.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        // customer giao dien sinh vien vao recyclerview
        View studentView = inflater.inflate(R.layout.customer_student, viewGroup, false);
        ViewHolder viewHolder = new ViewHolder(studentView);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        Student student = mListStudent.get(i);
        ViewHolder holder = (ViewHolder) viewHolder;
        holder.mNameStudent.setText( student.getName() + "");
        holder.mAgeStudent.setText(student.getAge() + "");


    }


    @Override
    public int getItemCount() {
        return mListStudent.size();
    }
    /** Lop cau truc cho View -- khi customer xlm vao RecyclerView*/
    public class ViewHolder extends RecyclerView.ViewHolder {
        View view;
        TextView mNameStudent;
        TextView mAgeStudent;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            view = itemView;
            mNameStudent = itemView.findViewById(R.id.name);
            mAgeStudent = itemView.findViewById(R.id.age);

        }
    }
}
