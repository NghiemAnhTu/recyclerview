package com.rikkei.tranning.ui;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    ArrayList<Student> mList;
    StudentAdapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mList = new ArrayList<Student>();
        recyclerView = findViewById(R.id.recyclerView);
        for ( int i = 0; i < 10; i++ ){
            Student student = new Student();
            student.setAge(i + 10);
            student.setName("Nguyen Van " + i );
            mList.add(student);
        }

        adapter = new StudentAdapter(mList, this);

        // LinearLayoutManager
        // Theo chieu doc
        //LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        // Theo chieu ngang
        //LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);

        // GridLayoutManager
        GridLayoutManager layoutManager = new GridLayoutManager(this,2);

        //StaggeredGridLayoutManager
        //StaggeredGridLayoutManager layoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(layoutManager);



    }
}
